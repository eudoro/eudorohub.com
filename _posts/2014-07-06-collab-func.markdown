---
layout: post
title:  Cómo colaborar como funcional
date:   2014-07-July
lang:   es
author:
  name: Manuel Vega Ulloa 
  github: eudoro
categories: es func
section: collab
---
Si usted es funcional, podrá colaborar de varias maneras. <!-- more -->

  * Creando nuevos asuntos para encarar nuevas funcionalidades.
  * Ejecutando test funcionales.
  * Planteando nuevos diseños.

